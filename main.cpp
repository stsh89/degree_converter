#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

bool check_doubles(double a, double b, double epsilon) {
  return (fabs(a - b)) <= (epsilon * fabs(a));
}

double from_dms(int degrees, int minutes, int seconds) {
  return double(degrees) +
    (double(minutes) / double(60)) +
    (double(seconds) / double(3600));
}

void test_from_dms_1() {
  cout << "from_dms(37, 51, 19)...   ";

  double got = from_dms(37, 51, 19);
  double want = 37.85527778;

  std::cout << fixed;
  std::cout << setprecision(8);

  if (check_doubles(got, want, 0.0000000001)) {
    cout << "ok\n";
  } else {
    cout << "error, want: " << want << ", got: " << got << endl;
  }
}

int main() {
  test_from_dms_1();

  return 0;
}

